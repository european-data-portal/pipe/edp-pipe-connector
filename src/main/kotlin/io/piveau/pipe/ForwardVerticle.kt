package io.piveau.pipe

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.Json
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.predicate.ResponsePredicate
import org.slf4j.LoggerFactory
import java.net.URL

class ForwardVerticle : AbstractVerticle() {
    private val log = LoggerFactory.getLogger(this.javaClass)

    private lateinit var webClient: WebClient

    override fun start(startPromise: Promise<Void>) {
        vertx.eventBus().consumer(ADDRESS, this::handleForward)
        webClient = WebClient.create(vertx)
        startPromise.complete()
    }

    private fun handleForward(message: Message<Buffer>)  {
        val pipeManager = PipeManager.read(message.body().toString())
        pipeManager.currentEndpoint?.let {
            val address = URL(it.address)
            val method = HttpMethod.valueOf(it.method ?: "POST")
            webClient.request(method, address.port, address.host, address.path)
                .putHeader("Content-Type", "application/json")
                .expect(ResponsePredicate.SC_SUCCESS)
                .sendBuffer(Buffer.buffer(pipeManager.prettyPrint())) { ar ->
                    if (ar.succeeded()) {
                        log.trace("Successfully forwarded:\n{}", pipeManager.prettyPrint())
                    } else {
                        log.error("Forward request", ar.cause())
                    }
                }
        } ?: log.trace("No next pipe segment available")
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.forward.queue"
    }

}