package io.piveau.pipe

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.piveau.pipe.connector.PipeConnector
import io.piveau.pipe.model.DataType
import io.piveau.pipe.model.Endpoint
import io.piveau.pipe.model.Pipe
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import java.net.URL

class PipeContext private constructor(content: String, private val pipeConnector: PipeConnector? = null) {

    val pipeManager = PipeManager.read(content)

    private val pipeLogger = PipeLogger(pipe.header, pipeManager.currentSegment!!.header)

    @JvmOverloads
    constructor(
        jsonObject: JsonObject,
        pipeConnector: PipeConnector? = null
    ) : this(jsonObject.encode(), pipeConnector)

    @JvmOverloads
    constructor(buffer: Buffer, pipeConnector: PipeConnector? = null) : this(
        buffer.toJsonObject(), pipeConnector
    )

    var forwarded: Boolean = false

    fun isForwarded(): Boolean = forwarded

    private var failureThrowable: Throwable? = null

    val failure: Boolean
        get() = failureThrowable != null

    fun isFailure(): Boolean = failure

    val cause: Throwable?
        get() = failureThrowable

    private var textResult: String? = null
    private var byteResult: ByteArray? = null

    private var mimeTypeResult: String? = null

    private var dataInfoResult: ObjectNode? = null

    val config: JsonObject
        get() = JsonObject(pipeManager.config.toString())

    val mimeType: String?
        get() = pipeManager.mimeType

    val dataInfo: JsonObject
        get() = JsonObject(pipeManager.dataInfo.toString())

    val pipe: Pipe
        get() = pipeManager.pipe

    val log: PipeLogger
        get() = pipeLogger

    fun log() = pipeLogger

    val nextEndpoint: Endpoint?
        get() = pipeManager.nextEndpoint

    val stringData: String
        get() = pipeManager.data

    val binaryData: ByteArray
        get() = pipeManager.binaryData

    val message: JsonObject
        get() {
            val message = JsonObject().put("config", pipeManager.config)
            if (pipeManager.isBase64Payload) {
                message.put("data", pipeManager.binaryData).put("dataType", DataType.base64)
            } else {
                message.put("data", pipeManager.data).put("dataType", DataType.text)
            }
            return message
        }

    @JvmOverloads
    fun setResult(result: String, dataMimeType: String? = null, info: JsonObject): PipeContext =
        setResult(result, dataMimeType, ObjectMapper().readTree(info.encode()) as ObjectNode)

    @JvmOverloads
    fun setResult(result: String, dataMimeType: String? = null, info: ObjectNode? = null): PipeContext {
        textResult = result
        mimeTypeResult = dataMimeType
        dataInfoResult = info
        forwarded = false
        return this
    }

    @JvmOverloads
    fun setResult(result: ByteArray, dataMimeType: String? = null, info: JsonObject): PipeContext =
        setResult(result, dataMimeType, ObjectMapper().readTree(info.encode()) as ObjectNode)

    @JvmOverloads
    fun setResult(result: ByteArray, dataMimeType: String? = null, info: ObjectNode? = null): PipeContext {
        byteResult = result
        mimeTypeResult = dataMimeType
        dataInfoResult = info
        forwarded = false
        return this
    }

    fun getFinalPipe(): Pipe {
        textResult?.let {
            pipeManager.setPayloadData(it, DataType.text, mimeTypeResult, dataInfoResult)
        } ?: byteResult?.let {
            pipeManager.setPayloadData(it, mimeTypeResult, dataInfoResult)
        }
        return pipeManager.getProcessedPipe()
    }

    fun getFinalBuffer(): Buffer? = Buffer.buffer(PipeManager.manage(getFinalPipe()).prettyPrint())

    fun forward() = pipeConnector?.forward(this) ?: Unit

    fun forward(webClient: WebClient) {
        if (forwarded) {
            log.warn("Already forwarded")
            return
        }

        nextEndpoint?.let { endpoint ->
            getFinalBuffer().let { pipe ->
                val address = URL(endpoint.address)
                val method = HttpMethod.valueOf(endpoint.method ?: "POST")
                webClient.request(method, address.port, address.host, address.path)
                    .putHeader("Content-Type", "application/json")
                    .sendBuffer(pipe) {
                        if (it.succeeded()) {
                            if (it.result().statusCode() == 200 || it.result().statusCode() == 202) {
                                log.trace("successfully forwarded")
                            } else {
                                log.error("{} - {}", it.result().statusCode(), it.result().statusMessage())
                            }
                        } else {
                            log.error("Forward request", it.cause())
                        }
                    }
            }
        } ?: log.trace("No next pipe segment available")

        forwarded = true
    }

    fun pass() = pipeConnector?.pass(this) ?: Unit

    fun pass(webClient: WebClient) {
        if (forwarded) {
            log.warn("Already forwarded")
            return
        }

        if (pipeManager.isBase64Payload) {
            setResult(pipeManager.binaryData, pipeManager.mimeType, pipeManager.dataInfo)
        } else {
            setResult(pipeManager.data, pipeManager.mimeType, pipeManager.dataInfo)
        }
        forward(webClient)
    }

    fun setFailure(message: String) = setFailure(Throwable(message))

    fun setFailure(throwable: Throwable): PipeContext {
        failureThrowable = throwable

        log.error("Pipe failure: " + throwable.message, throwable.cause ?: throwable)

        if (pipeConnector?.isMailerEnabled == true) {
            pipeConnector.useMailer()?.send(this)
        }
        return this
    }

}
