package io.piveau.pipe.connector

import io.piveau.pipe.ForwardVerticle
import io.piveau.pipe.PipeContext
import io.piveau.pipe.PipeMailer
import io.piveau.pipe.PipeMailer.Companion.create
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.*
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.dropwizard.MetricsService
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.api.contract.RouterFactoryOptions
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.StaticHandler
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.stream.Collectors
import java.util.stream.Stream


/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @return Future of the connector
 */
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions()
) = PipeConnector.create(vertx, deploymentOptions)

/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @param handler Handler function which is called when the creation finished
 * @return Future of the connector
 */
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions(),
    handler: (AsyncResult<PipeConnector>) -> Unit
) = createPipeConnector(vertx, deploymentOptions).onComplete(handler)

/**
 * Create a pipe connector
 *
 * @param vertx The vertx instance
 * @param deploymentOptions The deployment options for the internally used verticle
 * @param handler Handler object which is called when the creation finished
 * @return Future of the connector
 */
fun createPipeConnector(
    vertx: Vertx = Vertx.vertx(),
    deploymentOptions: DeploymentOptions = DeploymentOptions(),
    handler: Handler<AsyncResult<PipeConnector>>
) = createPipeConnector(vertx, deploymentOptions).onComplete(handler)

/**
 * The pipe connector
 *
 * The connector opens an http endpoint for receiving pipes. When a pipe is received, the connector either calls a
 * previously installed handler or publish it to a specific address of the Vert.x event bus.
 */
class PipeConnector : AbstractVerticle() {

    private var handler: ((PipeContext) -> Unit)? = null

    private var consumerAddress: String? = null

    private var pipeMailer: PipeMailer? = null

    private lateinit var router: Router
    private lateinit var client: WebClient

    override fun start(startPromise: Promise<Void>) {
        val metrics = MetricsService.create(vertx)

        client = WebClient.create(vertx)

        val storeOptions = ConfigStoreOptions()
            .setType("env")
            .setConfig(
                JsonObject().put(
                    "keys", JsonArray()
                        .add("PIVEAU_PIPE_ENDPOINT_PORT")
                        .add("PIVEAU_PIPE_MAIL_CONFIG")
                )
            )

        val retriever = ConfigRetriever.create(vertx, ConfigRetrieverOptions().addStore(storeOptions))
        retriever.getConfig { lc: AsyncResult<JsonObject> ->
            if (lc.succeeded()) {
                val config = lc.result()
                if (config.containsKey("PIVEAU_PIPE_MAIL_CONFIG")) {
                    val mailConfig = config.getJsonObject("PIVEAU_PIPE_MAIL_CONFIG")
                    pipeMailer = create(vertx, mailConfig)
                }
                OpenAPI3RouterFactory.create(
                    vertx,
                    "webroot/openapi-pipe.yaml"
                ) { ar: AsyncResult<OpenAPI3RouterFactory> ->
                    if (ar.succeeded()) {
                        val routerFactory =
                            ar.result()
                        val options =
                            RouterFactoryOptions().setMountNotImplementedHandler(true)
                        routerFactory.options = options
                        routerFactory.addHandlerByOperationId(
                            "incomingPipe"
                        ) { routingContext: RoutingContext ->
                            incomingPipeHandler(routingContext)
                        }
                        routerFactory.addHandlerByOperationId(
                            "configSchema"
                        ) { routingContext: RoutingContext ->
                            configSchemaHandler(routingContext)
                        }
                        router = routerFactory.router
                        router.route().order(0).handler(
                            CorsHandler.create("*").allowedMethods(
                                Stream.of(HttpMethod.POST)
                                    .collect(Collectors.toSet())
                            )
                        )
                        router.get("/pipe/schema").handler(this::pipeSchemaHandler)
                        router.route("/*").handler(StaticHandler.create())
                        val hch =
                            HealthCheckHandler.create(vertx)
                        hch.register(
                            "buildInfo"
                        ) { future: Promise<Status?> ->
                            vertx.fileSystem().readFile(
                                "buildInfo.json"
                            ) { bi: AsyncResult<Buffer> ->
                                if (bi.succeeded()) {
                                    future.complete(
                                        Status.OK(
                                            bi.result().toJsonObject()
                                        )
                                    )
                                } else {
                                    future.fail(bi.cause())
                                }
                            }
                        }
                        router.get("/health").handler(hch)
                        router.get("/metrics").produces("application/json")
                            .handler { routingContext: RoutingContext ->
                                val m = metrics.getMetricsSnapshot(vertx)
                                if (m != null) {
                                    routingContext.response().setStatusCode(200).end(m.encodePrettily())
                                } else {
                                    routingContext.response().setStatusCode(503).end()
                                }
                            }
                        val server = vertx.createHttpServer(
                            HttpServerOptions().setPort(
                                config.getInteger(
                                    "PIVEAU_PIPE_ENDPOINT_PORT",
                                    8080
                                )
                            )
                        )
                        server.requestHandler(router).listen()
                        startPromise.complete()
                    } else {
                        startPromise.fail(ar.cause())
                    }
                }
            }
        }
    }

    /**
     * Set handler for incoming pipe objects
     *
     * @param handler Handler function that will be called when the connector receives a pipe
     */
    fun handler(handler: (PipeContext) -> Unit) {
        this.handler = handler
        consumerAddress = null
    }

    /**
     * Set handler for incoming pipe objects
     *
     * @param handler Handler object that will be called when the connector receives a pipe
     */
    fun handler(handler: Handler<PipeContext>) = handler(handler::handle)

    @Deprecated(message = "Name is ambiguous", replaceWith = ReplaceWith("publishTo(address)"))
    fun consumer(consumer: String) = publishTo(consumer)

    fun publishTo(address: String) {
        this.consumerAddress = address
        handler = null
    }

    fun subRouter(mountPoint: String?, router: Router) = this.router.mountSubRouter(mountPoint, router)

    private fun incomingPipeHandler(routingContext: RoutingContext) {
        val pipe = routingContext.bodyAsJson
        with(pipe) {
            if (!getJsonObject("header").containsKey("runId")) {
                getJsonObject("header").put("runId", UUID.randomUUID().toString())
            }
            if (!getJsonObject("header").containsKey("startTime")) {
                getJsonObject("header").put(
                    "startTime",
                    Date().toInstant().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                )
            }
        }

        when {
            consumerAddress != null -> {
                vertx.eventBus()
                    .publish(
                        consumerAddress,
                        pipe,
                        DeliveryOptions().setCodecName(PipeContextMessageCodec(this).name())
                    )
                routingContext.response().setStatusCode(202).end(pipe.getJsonObject("header").getString("runId"))
            }
            handler != null -> {
                val pipeContext = PipeContext(pipe, this)
                vertx.executeBlocking({ promise: Promise<Void> ->
                    handler?.invoke(pipeContext)
                    if (pipeContext.isFailure()) {
                        promise.fail(pipeContext.cause)
                    } else {
                        if (!pipeContext.isForwarded()) {
                            pipeContext.forward(client)
                        }
                        promise.complete()
                    }
                }) { result: AsyncResult<Void> ->
                    if (result.succeeded()) {
                        pipeContext.log().trace("Incoming pipe successfully handled by handler.")
                    } else {
                        pipeContext.log().error("Handling pipe", result.cause())
                    }
                }
                routingContext.response().setStatusCode(202).end(pipe.getJsonObject("header").getString("runId"))
            }
            else -> {
                routingContext.response().setStatusCode(501).end()
            }
        }
    }

    private fun configSchemaHandler(routingContext: RoutingContext) {
        vertx.fileSystem().readFile("config.schema.json") {
            if (it.succeeded()) {
                routingContext.response().putHeader("Content-Type", "application/schema+json").end(it.result())
            } else {
                routingContext.response().setStatusCode(404).end()
            }
        }
    }

    private fun pipeSchemaHandler(routingContext: RoutingContext) {
        vertx.fileSystem().readFile("piveau-pipe.schema.json") {
            if (it.succeeded()) {
                routingContext.response().putHeader("Content-Type", "application/schema+json").end(it.result())
            } else {
                routingContext.response().setStatusCode(404).end()
            }
        }
    }

    fun forward(pipeContext: PipeContext) {
        if (!pipeContext.isForwarded()) {
            vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
        }
    }

    fun pass(pipeContext: PipeContext) {
        if (!pipeContext.isForwarded()) {
            if (pipeContext.pipeManager.isBase64Payload) {
                pipeContext.setResult(pipeContext.binaryData, pipeContext.mimeType, pipeContext.dataInfo)
            } else {
                pipeContext.setResult(pipeContext.stringData, pipeContext.mimeType, pipeContext.dataInfo)
            }
            vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
        }
    }

    val isMailerEnabled: Boolean
        get() = pipeMailer != null

    fun useMailer(): PipeMailer? = pipeMailer

    companion object {
        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions()
        ): Future<PipeConnector> {
            val pipeConnector = PipeConnector()
            vertx.eventBus().registerCodec(PipeContextMessageCodec(pipeConnector))
            val promise = Promise.promise<PipeConnector>()

            val connectorPromise = Promise.promise<String>()
            vertx.deployVerticle(pipeConnector, deploymentOptions, connectorPromise)

            val forwardPromise = Promise.promise<String>()
            vertx.deployVerticle(ForwardVerticle(), deploymentOptions, forwardPromise)

            CompositeFuture.join(connectorPromise.future(), forwardPromise.future()).onSuccess {
                promise.complete(pipeConnector)
            }.onFailure { promise.fail(it) }

            return promise.future()
        }

        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions(),
            handler: (AsyncResult<PipeConnector>) -> Unit
        ): Future<PipeConnector> = create(vertx, deploymentOptions).onComplete(handler)

        @JvmStatic
        @JvmOverloads
        fun create(
            vertx: Vertx = Vertx.vertx(),
            deploymentOptions: DeploymentOptions = DeploymentOptions(),
            handler: Handler<AsyncResult<PipeConnector>>
        ): Future<PipeConnector> = create(vertx, deploymentOptions).onComplete(handler)
    }

}