package io.piveau.pipe.connector

import io.piveau.pipe.PipeContext
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec
import io.vertx.core.json.JsonObject

class PipeContextMessageCodec(private val pipeConnector: PipeConnector) : MessageCodec<JsonObject?, PipeContext?> {

    override fun encodeToWire(buffer: Buffer, entries: JsonObject?) {}

    override fun decodeFromWire(i: Int, buffer: Buffer): PipeContext? = null

    override fun transform(entries: JsonObject?): PipeContext? =
        PipeContext(entries!!, pipeConnector)

    override fun name(): String = javaClass.simpleName

    override fun systemCodecID(): Byte = -1

}