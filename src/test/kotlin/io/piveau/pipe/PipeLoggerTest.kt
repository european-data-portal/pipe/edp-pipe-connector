package io.piveau.pipe

import com.fasterxml.jackson.databind.ObjectMapper
import io.piveau.pipe.model.Pipe
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.IOException

@DisplayName("Testing the pipe logger")
@ExtendWith(VertxExtension::class)
class PipeLoggerTest {

    @Test
    @DisplayName("Testing log")
    fun logTest(vertx: Vertx, testContext: VertxTestContext) {
        vertx.fileSystem().readFile("src/test/resources/test-pipe.json") {
            if (it.succeeded()) {
                try {
                    val (header, body) = ObjectMapper().readValue(it.result().toString(), Pipe::class.java)
                    val logger = PipeLogger(header, body.segments[0].header)
                    logger.info("Test")
                    logger.info("Test {}", "param1")
                    logger.info("Test {}, {}", "param1", "param2")
                    logger.info("Test {}, {}, {}", "param1", "param2", "param3")
                    logger.info("Test", Throwable("throwable1"))
                    testContext.completeNow()
                } catch (e: IOException) {
                    testContext.failNow(e)
                }
            } else {
                testContext.failNow(it.cause())
            }
        }
    }

}