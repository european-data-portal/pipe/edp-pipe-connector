package io.piveau.pipe.connector

import io.piveau.pipe.PipeContext
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing the pipe connector")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PipeConnectorTest {

    var connector: PipeConnector? = null

    @BeforeAll
    @DisplayName("Initialize connector")
    fun createConnector(vertx: Vertx, testContext: VertxTestContext) {
        PipeConnector.create(vertx) {
            if (it.succeeded()) {
                connector = it.result()
                testContext.completeNow()
            } else {
                testContext.failNow(it.cause())
            }
        }
    }

    @Test
    @DisplayName("Testing handler")
    fun testHandler(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint()
        connector!!.handler(Handler { pipeContext: PipeContext ->
            println(pipeContext.pipe.header.title)
            checkpoint.flag()
        })
        webInjection(vertx, testContext, "test-pipe.json")
    }

    @Test
    @DisplayName("Testing producer")
    fun testProducer(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint()
        vertx.eventBus().consumer(
            "io.piveau.pipe.connector.testing"
        ) { msg: Message<PipeContext> ->
            val pipeContext = msg.body()
            println(pipeContext.pipe.header.title)
            checkpoint.flag()
            msg.reply("success")
        }
        connector?.consumer("io.piveau.pipe.connector.testing")
        webInjection(vertx, testContext, "test-pipe.json")
    }

    private fun webInjection(
        vertx: Vertx,
        testContext: VertxTestContext,
        fileName: String
    ) {
        val buffer = vertx.fileSystem().readFileBlocking(fileName)
        val pipe = JsonObject(buffer)
        val client = WebClient.create(vertx)
        client.post(8080, "localhost", "/pipe")
            .putHeader("content-type", "application/json")
            .sendJsonObject(
                pipe
            ) { response: AsyncResult<HttpResponse<Buffer?>> ->
                if (response.succeeded()) {
                    if (response.result().statusCode() == 202) {
                        testContext.completeNow()
                    } else {
                        testContext.failNow(Throwable(response.result().statusMessage()))
                    }
                } else {
                    testContext.failNow(response.cause())
                }
            }
    }

}